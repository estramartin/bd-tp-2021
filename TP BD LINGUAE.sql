
create table sedes(
	 cod_sede varchar(4) primary key not null,
	 provincia varchar(20) not null ,
	 cuidad varchar(20) not null ,
	 direccion varchar(50) not null unique,
	 telefono varchar(15) unique

)

--------------------------------------------------


create table cursos(

	cod_idioma varchar(4) primary key not null,
	nombre varchar(10),
	nivel varchar(3) not null

	
)

---------------------------------------------------


create table personas(

	dni varchar(9) primary key not null,
	nombre varchar(50) not null,
	apellido varchar(50) not null,
	fecha_nac date not null,
	direccion varchar(50),
	telefono varchar(15)
)

-----------------------------------------------



create domain dom_puestos varchar(2) not null check(value in('DG','DI','SI','DO','EV','AL')) 
create domain dom_fecha_cargos date not null check(value >= current_date )


create table cargos(

	dni_persona varchar(9) references personas(dni),
	cod_sede  varchar(4) references sedes(cod_sede),
	nom_curso_idioma varchar(10),
	puesto dom_puestos,
	fecha_inicio dom_fecha_cargos,
	fecha_fin date	
)



------------------------------------------------------

create domain dom_fecha_eva date not null check(value >= current_date)

create table evaluaciones(

     nro_eval serial primary key,
	 dni_docente_evaluador varchar(9) references personas(dni),
	 cod_cursoidioma varchar(4) references cursos(cod_idioma),
	 fecha dom_fecha_eva
)


------------------------------------------------------------
create domain dom_nota int check(value >=1 and value <= 10)

create table eval_alumnos(

	dni_alumno varchar(9) references personas(dni),
	nro_eval int references evaluaciones(nro_eval),
	nota dom_nota,
	fecha date not null,
	ausente bool not null
	
)

----------------------------------------------------------

create domain dom_fechaini_curso_alumnos date not null check(value >= current_date)


create table curso_alumnos(

	dni_alumno varchar(9) references personas(dni),
	cod_curso_idioma varchar(4) references cursos(cod_idioma),
	dia varchar(10) not null check( dia in('Lunes','Martes','Miercoles','Jueves', 'Viernes')),
	hora time not null,
	fecha_inicio date,
	fecha_fin date
)

------------------------------------------------------------
create domain dom_fechaini_curso_docentes date not null check(value >= current_date)


create table curso_docentes(

	dni_docente varchar(9)  references personas(dni),
	cod_curso_idioma varchar(4) references cursos(cod_idioma),
	dia varchar(10) not null check(dia in('Lunes','Martes','Miercoles','Jueves', 'Viernes')),
	hora time not null,
	fecha_inicio date,
	fecha_fin date
)



-------------------------------------------------------------

create domain dom_monto decimal(10,2) not null check( value > 0 )

create table pagos(

	nro_comprobante serial primary key,
	cod_sede varchar(4) references sedes(cod_sede),
	cod_cursoidioma varchar(4) references cursos(cod_idioma),
	dni_alumno varchar(9) references personas(dni),
	fecha date default(current_date),
	monto dom_monto
)


------------------------------------------CARGA VALORES-------------------------------------------------------

insert into sedes values
('3100', 'Entre Rios', 'Paraná', 'Necochea 553', '+543434242791'),
('2000', 'Santa Fe', 'Rosario', 'Peron 123', '+543414657802'),
('5500', 'Mendoza', 'Mendoza', 'San Martin 456', '+542614316280')

--------------------------------------------------------------------------------------------------

insert into cursos values
('E1P','Ingles', 'A1'),
('E2P','Ingles', 'A2'),
('E3P','Ingles', 'B1'),
('E4P','Ingles', 'B2'),
('E5P','Ingles', 'C1'),
('E6P','Ingles', 'C2'),
('A1P','Aleman', 'A1'),
('A2P','Aleman', 'A2'),
('A3P','Aleman', 'B1'),
('A4P','Aleman', 'B2'),
('A5P','Aleman', 'C1'),
('A6P','Aleman', 'C2'),
('I1P','Italiano', 'A1'),
('I2P','Italiano', 'A2'),
('I3P','Italiano', 'B1'),
('I4P','Italiano', 'B2'),
('I5P','Italiano', 'C1'),
('I6P','Italiano', 'C2'),

('E1V','Ingles', 'A1'),
('E2V','Ingles', 'A2'),
('E3V','Ingles', 'B1'),
('E4V','Ingles', 'B2'),
('E5V','Ingles', 'C1'),
('E6V','Ingles', 'C2'),
('A1V','Aleman', 'A1'),
('A2V','Aleman', 'A2'),
('A3V','Aleman', 'B1'),
('A4V','Aleman', 'B2'),
('A5V','Aleman', 'C1'),
('A6V','Aleman', 'C2'),
('I1V','Italiano', 'A1'),
('I2V','Italiano', 'A2'),
('I3V','Italiano', 'B1'),
('I4V','Italiano', 'B2'),
('I5V','Italiano', 'C1'),
('I6V','Italiano', 'C2')

--------------------------------------------------------------------------------------------------------



insert into personas values
('32831554', 'Martin','Estrada','21-02-1987','Alsina 869', '3434636602'),
('37080129', 'Analia Belén','Cuenca','03-09-1992','Garrigó 1675 ', '3435029905'),
('35706276', 'Alejandro','Ghiringhelli','02-04-1991','Diamante 267', '3436230863'),
('31484774','Alfredo Francisco', 'Machado','01/06/1985', 'BoJ. Hernandez Manz. 4 C 2',  '4390678'),
('30563991','Ezequiel Eduardo','Mandel','18/08/1962', 'Gessino 216 Casa 14 Mzna. 4','4354128' ),
('30796852', 'Ana Estela','Ortiz','24/06/1985','Chacabuco 484','154502946' ),
('17022037', 'Omar Eliseo','Pitrovski','23/11/1980', 'Lomas del Mirador II Sec. A2','4246192' ),
('30560373','Xavier Emmanuel','Ramirez','18/08/1974', 'J. J. Hernandez Cuerpo 6 Mzna. 4','15537238'),
('32114702','Dario', 'Renna','13/08/1982','Barrio VICOER XII Mzna. A','0344287348' ),
('26152765','Emmanuel','Retamar','07/10/1982', 'BoHernandarias', '0315614420'  ),
('29269732', 'Gabriel Eugenio','Sosa','26/03/1986', 'Villaguay', '4341273' ),
('35441479','Guido Leonel','Suarez Rios','24/07/1981','Ayacucho 564',null ),
('29855862', 'Patricio Jose','Taurisano','25/10/1984', 'Santiago Derqui 749','4374410'),
('29957038','Pascual Timoteo A.','Uriona','11/03/1990', 'BoParana I Mzna 1','154553802' ),
('22850572','Leandro Raul','Vacaretti','25/06/1981', 'Nicanor Molina 511','4340448' ),
('22160011','Baltazar Carlos Eduardo','Wolff','04/07/1955', 'Jose M. Paz 1097','156238333' ),
('31277508','Leonardo Javier','Caceres','02/11/1982', 'Santiago Derqui 749','4840430' ),
('32188553', 'Javier','Filgueira','26/01/1984', 'Barrio Parana XIV Mzna. C','4390676' ),
('29157553','Angel Santiago','Flores','16/07/1978', 'BoJ.M. de Rosas Mzna. C2 Dpto 1','155042142' ),
('27337776','Juan Manuel','Iglesias','13/03/1984', 'La Rioja 331', '154684007' ),
('31579005','Luis Alejo','Romani','30/03/1988', 'Antonio Machado 703','156230784' ),
('33865514','Juan Pablo','Yacob','03/03/1977', 'Houssay 415', '154292170'),
('29447624','Luciano Miguel','Yugdar','24/07/1983', 'Barrio Parana XIV','154520300' ),
('28913190','Cynthia Noemi','Zacarias','06/07/1978', 'Berutti 640','154291709' ),
('29447678','Ivan Sergio Raul','Zanin','19/08/1982', 'Nicanor Molina 511','154786824' ),
('29945156','Maximiliano Juan Jose','Zapata','29/09/1982', 'Dr. Gessino 556','154297028' ),
('35441056','Cristhian Orlando','Diana','14/01/1988', 'Cordoba 162','0315590264'),
('36813450', 'Gaston Edurado', 'Perez', '23/10/1992', 'Laprida 751', '02915408269'),
('30578551', 'Jonathan Fabricio', 'Osorio', '03/11/1983', 'Los Paraisos 75', '4975377'),
('24752862', 'Federico Ivan', 'Pagura', '03/07/1975', 'Alsina 637', '03156234277'),
('25089771', 'Raul Virgilio', 'Rodriguez', '04/02/1976', 'Monte Caseros 323', '4227355'),
('36813449', 'Walter German Ramon', 'Wurstten', '13/02/1982', 'Laprida 760', '156117046'),
('21698023', 'Leonardo Juan Jose', 'Almada', '27/05/1970', 'Los Paraisos 75', '155032229'),
('32932173', 'Hernan Dario', 'Schmittlein', '23/10/1992', 'Alsina 654', '03156356487'),
('25089776', 'Gonzalo Exequiel', 'Saavedra', '25/02/1988', 'Rep. Dominicana 552', '155091007'),
('30408500', 'Gabriel Edurado', 'Martinez', '23/10/1992', 'Laprida 758', '02915408269'),
('29447005', 'Paola Soledad ', 'Roble' , '27/03/1982', 'Saravi 117', '4373580'),
('34586153', 'Yamila Haydee', 'Quintana', '16/07/1989', 'Los Jacarandaes 1173', '154732280'),
('26564401', 'Cynthia Carina', 'Popelka', '17/07/1978', 'Nogoya 49', '4231234'),
 ('31313058', 'Juan Esteban','Piaggio','13/12/1984', 'Carrera y Vidal', NULL),
('25546396', 'Luis Maria', 'Piani', '26/11/1976', 'Moises Lebenshon 3426', '4351142'),
('34549524', 'Hugo Alejandro','Piantanida', '03/06/1989', 'Rio de los Pajaros 2716', '4331382'),
('37465542', 'Nicolas Daniel', 'Piccoli', '03/04/1993', 'Coronel Caminos 1329','4272812'),
('26519505', 'Soledad Lucrecia','Piccolo', '09/04/1978', 'Los Sauces 162', NULL),
('33838416', 'Alejandro Gabriel','Pico', '06/06/1988','Casiano Calderon 1888', '4272079'),
('27449041', 'Gabriel Jesus', 'Picotte', '10/10/1979', 'Virgen del Lujan 1951', '4271193'),
('29313095', 'Jorge Jesus','Picotte', '20/07/1982',  'Virgen del Lujan 1951', '4271193'),
('30196994', 'Maria De Los Angeles','Picotti', '25/02/1984', 'Pte. Peron 545', '4930302'),
('30322973', 'Matias Ismael','Picotti', '11/04/1984', '9 de julio 124',  '034156042'),
('28968427', 'Javier Luis','Pidone', '28/09/1981', 'Buenos Aires 441',  '4231109'),
('32509756', 'Ekaterina Micaela','Piergiovanni', '20/08/1986',  'Suipacha 1867', '4241228'),
('35706114', 'Miguel Martin','Piergiovanni', '13/03/1991',  'Gonzalez Pecotche 1896','4241228'),
('34586379', 'Mariano Hernan','Piffiguer', '04/09/1989', 'Dragones de Entre Rios 635', '154767142'),
('35448155', 'Juan Antonio','Pighin', '07/08/1990','A. Castellano 2097', '421779'),
('14330569', 'Walter Carlos', 'Pignatta', '05/09/1961', 'Ameghino 581','034432156'),
('29447407', 'Federico','Pilnik', '20/06/1982', 'San Martin 118','4314439'),
('32327660', 'Bruno Andres','Pimentel', '02/04/1986',  'Los Ceibos 869', '4271012'),
('35706249', 'Maria Florencia','Pimentel', '27/03/1991', 'Los Ceibos 869', '03470805'),
('14604391', 'Raul Ceferino','Pin', '29/01/1961','Rep. Dominicana 640','4340487'),
('35708167', 'Micaela Anabel','Pineiro', '28/07/1991', 'Guemes 255', '03438421396'),
('37292002', 'Pedro Gustavo', 'Pineyro Santucci', '15/10/1992', 'Zona Rural 0', '037420511'),
('20096213', 'Raul Francisco', 'Pintos Sors', '15/03/1968','Rosario del Tala 512', '4221686'),
('23996958', 'Claudio Fabian','Pintos', '09/09/1974', 'Alvarado 2684',  '156217841'),
('31232287', 'Juan Emanuel','Pintos', '21/11/1984', 'Bvar. Sarmiento 579',  '4221239'),
('34972735', 'Carlos Eduardo de Jesus', 'Piris',  '26/12/1989', 'Gobernador Antelo 1683',  '154407421'),
('31760315', 'Vanina Maria Alejandra', 'Pirola', '06/08/1985',  'Los Sauces 243',  '4260625'),
('27466071', 'Pablo Martin','Pittavino', '14/07/1979', 'Las Lechiguanas 597', '4243187'),
('29447170', 'Nestor Ivan','Pizzichini', '08/04/1982',  'Regis Martinez 1735', '4246437'),
('26203750', 'Marianela','Pizzio', '16/11/1977',  'Gob. Parera 985', '4260093'),
('34678065', 'Diego Emanuel', 'Planes', '04/07/1989', 'La Delfina 1441', '4953903' ),
('32802059', 'Leandro Enrique', 'Planes', '19/05/1987', 'Turi 97','155035854'),
('30175494', 'Federico','Plaumer', '11/12/1984',  'Las Lechiguanas 571','4247784'),
('24592441', 'Claudia Rosana','Plez', '23/07/1975','Berio Acosta 1625', '155449461'),
('21411913', 'Cristian Raul','Pochon', '05/01/1970', 'Espana 267', '154476178'),
('36269182', 'Bruno Javier', 'Podversich', '20/01/1992', 'Zona Rural 0', '034154526051'),
('33349258', 'Eugenio Ezequiel','Podversich', '12/05/1988', 'Gervacio Mendez 519', '154388815'),
('25307515', 'Fernan Humberto','Poidomani', '01/08/1976', 'Cordoba 740','4317337'),
('34452406', 'Adrian Eduardo', 'Polarrolo', '15/09/1989', 'Ecuador 138', '4910435'),
('28676029', 'Graciela Valeria','Politi', '11/02/1981', 'Av. de las Americas', '4350114'),
('31017257', 'Exequiel Maximiliano','Polito Acosta', '28/07/1984',   'Gendarmeria Nacional 1637','4030909'),
('34821454', 'Franco Joel', 'Polo', '12/02/1990','Iriondo 1581','0348229033'),
('32058807', 'Laura', 'Ponce Bossio', '20/07/1986','San Martin 772',  '4973301'),
('23190739', 'Lucrecia Margarita Mercedes','Ponce', '09/08/1973', 'Independencia 958', '4880490'),
('34549589', 'Pablo Cesar','Ponce', '21/07/1989',  'Badano 0','154623501'),
('33271242', 'Francisco','Pons', '20/10/1987', '3 de Febrero', '4248909'),
('35441206', 'Juan Carlos','Poos', '15/11/1990', 'Coronel Uzin 555', '4349858'),
('32114383', 'Exequiel Dario', 'Popp', '26/03/1986','Misiones 1341', '49503502'),
('33927404', 'Gisela Mariel','Popp', '29/05/1988',  'Pasaje Moreno 74', '154388247'),
('34678024', 'Ivana Gisel','Popp', '05/06/1989', 'Misiones 1341', '4953502'),
('35442295', 'Jennifer Stefania', 'Popp', '29/09/1990', 'Rodriguez Pena 1034', '155048353'),
('31439635', 'Luciano','Porcaro', '25/02/1985',  'Av. Zanni 891', '4240872'),
('20096170', 'Orlando Javier','Porchnek', '24/02/1968', 'Av. Ramirez 3361', '4245155'),
('34059089', 'Mayka Antonella','Portal', '17/06/1990',  'Houssay 968',  '4910534'),
('37080626', 'Fabiola Maria del Mar','Portillo', '30/09/1992',  'Repetto 3804','0345422566'),
('32188504', 'Fabian Daniel', 'Portnoy', '18/02/1986', 'Almafuerte 225', '4245874'),
('35441760', 'Guillermo Luis','Portnoy', '22/01/1991', 'Almafuerte 225', '156111159'),
('33424056', 'Emanuel Maria','Porto Pereira', '15/12/1987', 'French 530','03156209229'),
('35164380', 'Marlene', 'Porto Pereira', '25/07/1990', 'French 530', '4994079'),
('35446262', 'Cristian Ezequiel','Portorreal', '07/08/1990',  'Pellegrini 1443','034754233'),
('30185183', 'Rodrigo Daniel','Postigo Werbrouck', '25/06/1983',  'Churruarin 525', '03156309443'),
('35289179', 'Milton', 'Pozzo', '14/09/1990','Belgrano 2548',  '03496-428955'),
('31384596', 'Nicolas Nazareno','Pozzo', '08/05/1985', 'Rio de Janeiro 76', '034430337'),
('32833780', 'Pablo Emanuel','Prada', '20/06/1987',   'Tomas Guido 2177', '4373172'),
('36910091', 'Geronimo Fabian','Pradella', '13/05/1992',  'Piran 5825', '4270220'),
('35295276', 'Daniel', 'Prado', '13/05/1990', 'Cuba 56', '4225161'),
('27139929', 'Danilo Martin','Pralong', '21/03/1979',   'Courrege 241', '4229766'),
('32745392', 'Sebastian Efrain', 'Pralong', '12/02/1987',  'H. Irigoyen 521', '4218164'),
('36452907', 'Agustin','Prediger', '13/03/1992',   'BoSan Salvador Casa 212',  '2901431913'),
('34669622', 'German', 'Prediger', '28/09/1989', 'Barrio San Salvador 212',  '029431913'),
('26713163', 'Andrea Marisel', 'Preisz', '29/01/1979', 'Diaz Colodrero 414',  '154285095'),
('35442432', 'Ariel Fernando', 'Preisz', '09/09/1990', 'Alejo Peyret 313',  '4235446')



--------------------------------------------------------------------------------------

insert into cargos values
('17022037', null, null, 'DG', '20-03-2022' ),
('37080129', '3100', 'Ingles', 'DI', '10-03-2022' ),
('35706276', '3100', 'Aleman', 'DI' , '11-03-2022'),
('32831554', '3100', 'Italiano', 'DI' , '12-03-2022'),
('26564401', '2000', 'Ingles', 'DI', '13-03-2022' ),
('34586153', '2000', 'Aleman', 'DI' , '14-03-2022'),
('29447005', '2000', 'Italiano', 'DI' , '15-03-2022'),
('30408500', '5500', 'Ingles', 'DI' , '16-03-2022'),
('25089776', '5500', 'Aleman', 'DI' , '17-03-2022'),
('32932173', '5500', 'Italiano', 'DI' , '18-03-2022'),
('21698023', '3100', 'Ingles', 'SI' , '19-03-2022'),
('36813449', '3100', 'Aleman', 'SI' , '20-03-2022'),
('25089771', '3100', 'Italiano', 'SI' , '21-03-2022'),
('24752862', '2000', 'Ingles', 'SI', '22-03-2022' ),
('30578551', '2000', 'Aleman', 'SI', '23-03-2022' ),
('36813450', '2000', 'Italiano', 'SI' , '24-03-2022'),
('29447678', '5500', 'Ingles', 'SI', '25-03-2022' ),
('29945156', '5500', 'Aleman', 'SI', '26-03-2022' ),
('35441056', '5500', 'Italiano', 'SI', '27-03-2022' ),
--------------------------------------------------------------------------
('31484774','3100', 'Ingles', 'DO', '10-03-2022' ),
('30563991','3100', 'Ingles', 'DO', '10-03-2022' ),
('30796852', '3100', 'Ingles', 'EV', '10-03-2022' ),
('31313058', '3100', 'Ingles', 'EV', '10-03-2022' ),
('30560373', '3100', 'Aleman', 'DO' , '14-03-2022'),
('32114702', '3100', 'Aleman', 'DO' , '14-03-2022'),
('26152765', '3100', 'Aleman', 'EV' , '14-03-2022'),
('29269732',  '3100', 'Aleman', 'EV' , '14-03-2022'),
('35441479', '3100', 'Italiano', 'DO' , '14-03-2022'),
('29855862',  '3100', 'Italiano', 'DO' , '14-03-2022'),
('29957038', '3100', 'Italiano', 'EV' , '14-03-2022'),
('22850572', '3100', 'Italiano', 'EV' , '14-03-2022'),

('22160011', '2000', 'Ingles', 'DO' , '14-03-2022'),
('31277508', '2000', 'Ingles', 'DO' , '14-03-2022'),
('32188553', '2000', 'Ingles', 'EV', '10-03-2022' ),
('29157553', '2000', 'Ingles', 'EV', '10-03-2022' ),
('27337776', '2000', 'Aleman', 'DO' , '14-03-2022'),
('31579005', '2000', 'Aleman', 'DO' , '14-03-2022'),
('33865514','2000', 'Aleman', 'EV' , '14-03-2022'),
('29447624','2000', 'Aleman', 'EV' , '14-03-2022'),
('28913190','2000', 'Italiano', 'DO' , '14-03-2022'),
('31313058', '2000', 'Italiano', 'DO' , '14-03-2022'),
('25546396','2000', 'Italiano', 'EV' , '14-03-2022'),
('34549524','2000', 'Italiano', 'EV' , '14-03-2022'),

('37465542','5500', 'Ingles', 'DO', '10-03-2022' ),
('26519505','5500', 'Ingles', 'DO', '10-03-2022' ),
('33838416', '5500', 'Ingles', 'EV', '10-03-2022' ),
('27449041', '5500', 'Ingles', 'EV', '10-03-2022' ),
('29313095', '5500', 'Aleman', 'DO' , '14-03-2022'),
('30196994', '5500', 'Aleman', 'DO' , '14-03-2022'),
('30322973', '5500', 'Aleman', 'EV' , '14-03-2022'),
('32509756',  '5500', 'Aleman', 'EV' , '14-03-2022'),
('35706114', '5500', 'Italiano', 'DO' , '14-03-2022'),
('34586379',  '5500', 'Italiano', 'DO' , '14-03-2022'),
('35448155', '5500', 'Italiano', 'EV' , '14-03-2022'),
('14330569', '5500', 'Italiano', 'EV' , '14-03-2022')



-----------------------------------------------------------------------------------------------


insert into evaluaciones (dni_docente_evaluador,cod_cursoidioma,fecha) values 
('31484774','E1P', '10-12-2022' ),
('31484774','E2P','11-12-2022'),
('31484774','E3P','12-12-2022' ),
('31484774','E4P', '13-12-2022' ),
('30563991','E5P','14-12-2022' ),
('30563991','E6P', '15-12-2022' ),
('30560373', 'A1P', '18-12-2022'),
('30560373', 'A2P', '19-12-2022'),
('30560373', 'A3P', '20-12-2022'),
('30560373', 'A4P', '21-12-2022'),
('32114702', 'A5P', '22-12-2022'),
('32114702', 'A6P', '10-12-2022'),
('35441479', 'I1P', '11-12-2022'),
('35441479', 'I2P', '12-12-2022'),
('35441479', 'I3P',  '13-12-2022'),
('35441479', 'I4P', '14-12-2022'),
('29855862',  'I5P', '15-12-2022'),
('29855862',  'I6P', '18-12-2022')



--------------------------------------------------------------------------------------------------


Insert into curso_alumnos values
('29447407', 'E1P', 'Lunes','20:00', '04-03-2021', null),
('32327660', 'E1P', 'Lunes','20:00', '04-03-2021', null),
('20096170', 'E1P', 'Lunes','20:00', '04-03-2021', null),
('34059089', 'E1P', 'Lunes','20:00', '04-03-2021', null),

('35706249', 'E2P', 'Lunes','22:00', '04-03-2021', null),
('14604391', 'E2P', 'Lunes','22:00', '04-03-2021', null),
('35164380', 'E2P', 'Lunes','22:00', '04-03-2021', null),

('35708167', 'E3V', 'Martes','20:00', '04-03-2021', null),
('37292002', 'E3V', 'Martes','20:00', '04-03-2021', null),
('37080626', 'E3V', 'Martes','20:00', '04-03-2021', null),
('32188504', 'E3V', 'Martes','20:00', '04-03-2021', null),

('20096213', 'E4V', 'Martes','22:00', '04-03-2021', null),
('23996958', 'E4V', 'Martes','22:00', '04-03-2021', null),
('35446262', 'E4V', 'Martes','22:00', '04-03-2021', null),

('31232287', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
('34972735', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
('35441760', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
('33424056', 'E5P', 'Miercoles','20:00', '04-03-2021', null),

('31760315', 'E6P', 'Miercoles','22:00', '04-03-2021', null),
('27466071', 'E6P', 'Miercoles','22:00', '04-03-2021', null),

('29447170', 'A1V', 'Jueves','20:00', '04-03-2021', null),
('26203750', 'A1V', 'Jueves','20:00', '04-03-2021', null),
('36910091', 'A1V', 'Jueves','20:00', '04-03-2021', null),
('35295276', 'A1V', 'Jueves','20:00', '04-03-2021', null),

('34678065', 'A2P', 'Jueves','22:00', '04-03-2021', null),
('32802059', 'A2P', 'Jueves','22:00', '04-03-2021', null),


('30175494','A3P', 'Viernes','20:00', '04-03-2021', null),
('24592441','A3P', 'Viernes','20:00', '04-03-2021', null),
('36452907', 'A3P', 'Viernes','20:00', '04-03-2021', null),
('34669622', 'A3P', 'Viernes','20:00', '04-03-2021', null),

('26713163', 'A4V', 'Viernes','22:00', '04-03-2021', null),
('35442432', 'A4V', 'Viernes','22:00', '04-03-2021', null),
('21411913', 'A4V', 'Viernes','22:00', '04-03-2021', null),
('36269182', 'A4V', 'Viernes','22:00', '04-03-2021', null),

('33349258', 'A5V', 'Lunes','17:00', '04-03-2021', null),
('25307515', 'A5V', 'Lunes','17:00', '04-03-2021', null),
('30185183', 'A5V', 'Lunes','17:00', '04-03-2021', null),
('35289179', 'A5V', 'Lunes','17:00', '04-03-2021', null),

('34452406', 'A6P', 'Lunes','19:00', '04-03-2021', null),
('28676029', 'A6P', 'Lunes','19:00', '04-03-2021', null),

('31017257', 'I1P', 'Martes','19:00', '04-03-2021', null),
('34821454', 'I1P', 'Martes','19:00', '04-03-2021', null),
('31384596', 'I1P', 'Martes','19:00', '04-03-2021', null),
('32833780', 'I1P', 'Martes','19:00', '04-03-2021', null),

('32058807', 'I2V', 'Martes','17:00', '04-03-2021', null),
('23190739', 'I2V', 'Martes','17:00', '04-03-2021', null),

('34549589', 'I3V', 'Miercoles','17:00', '04-03-2021', null),
('33271242', 'I3V', 'Miercoles','17:00', '04-03-2021', null),

('35441206', 'I4V', 'Miercoles','19:00', '04-03-2021', null),
('32114383', 'I4V', 'Miercoles','19:00', '04-03-2021', null),

('33927404', 'I5P', 'Jueves','17:00', '04-03-2021', null),
('34678024', 'I5P', 'Jueves','17:00', '04-03-2021', null),
('27139929', 'I5P', 'Jueves','17:00', '04-03-2021', null),
('32745392', 'I5P', 'Jueves','17:00', '04-03-2021', null),

('35442295', 'I6V', 'Jueves','19:00', '04-03-2021', null),
('31439635', 'I6V', 'Jueves','19:00', '04-03-2021', null)


-------------------------------------------------------------------------------------------------

insert into curso_docentes values
--parana
('31484774','E1P','Lunes','20:00', '10-03-2021', null ),
		('31484774','E2P','Lunes','22:00', '10-03-2021', null ),
		('31484774','E3V','Martes','20:00', '10-03-2021', null ),
		('31484774','E4V','Martes','22:00', '10-03-2021', null ),
		('30563991','E5P','Miercoles','20:00' ,'10-03-2021' , null),
		('30563991','E6P','Miercoles','22:00', '10-03-2021', null ),
		('30560373', 'A1V' ,'Jueves','20:00', '14-03-2021', null),
		('30560373', 'A2P' ,'Jueves','22:00', '14-03-2021', null),
		('30560373', 'A3P' ,'Viernes','20:00', '14-03-2021', null),
		('30560373', 'A4V' ,'Viernes','22:00', '14-03-2021', null),
		('32114702', 'A5V' ,'Lunes','17:00', '14-03-2021', null),
		('32114702', 'A6P' ,'Lunes','19:00', '14-03-2021', null),
		('35441479', 'I1P', 'Martes','19:00', '14-03-2021', null),
		('35441479', 'I2V', 'Martes','17:00', '14-03-2021', null),
		('35441479', 'I3V','Miercoles','17:00',  '14-03-2021', null),
		('35441479', 'I4V', 'Miercoles','19:00', '14-03-2021', null),
		('29855862',  'I5V' ,'Jueves','17:00', '14-03-2021', null),
		('29855862',  'I6V' ,'Jueves','19:00', '14-03-2021', null),
		--Rosario
		('22160011','E1P','Lunes','20:00', '10-03-2021', null ),
		('22160011','E2P', 'Lunes','22:00', '10-03-2021', null ),
		('22160011','E3P', 'Martes','20:00', '10-03-2021', null ),
		('22160011','E4P', 'Martes','22:00', '10-03-2021', null ),
		('31277508','E5P', 'Miercoles','20:00' ,'10-03-2021' , null),
		('31277508','E6P', 'Miercoles','22:00', '10-03-2021', null ),
		('27337776', 'A1V','Jueves','20:00', '14-03-2021', null),
		('27337776', 'A2V','Jueves','22:00', '14-03-2021', null),
		('27337776', 'A3V','Viernes','20:00', '14-03-2021', null),
		('27337776', 'A4V','Viernes','22:00', '14-03-2021', null),
		('31579005', 'A5V' ,'Lunes','17:00', '14-03-2021', null),
		('31579005', 'A6V' ,'Lunes','19:00', '14-03-2021', null),
		('35441479', 'I1P',  'Martes','19:00', '14-03-2021', null),
		('35441479', 'I2P',  'Martes','17:00', '14-03-2021', null),
		('35441479', 'I3P', 'Miercoles','17:00',  '14-03-2021', null),
		('35441479', 'I4P',  'Miercoles','19:00', '14-03-2021', null),
		('29855862',  'I5P', 'Jueves','17:00', '14-03-2021', null),
		('29855862',  'I6P', 'Jueves','19:00', '14-03-2021', null),
		--mendoza
		('37465542','E1V','Lunes','20:00', '10-03-2021', null ),
		('37465542','E2V', 'Lunes','22:00', '10-03-2021', null ),
		('37465542','E3V', 'Martes','20:00', '10-03-2021', null ),
		('37465542','E4V', 'Martes','22:00', '10-03-2021', null ),
		('26519505','E5V', 'Miercoles','20:00' ,'10-03-2021' , null),
		('26519505','E6V', 'Miercoles','22:00', '10-03-2021', null ),
		('29313095', 'A1P','Jueves','20:00', '14-03-2021', null),
		('29313095', 'A2P','Jueves','22:00', '14-03-2021', null),
		('29313095', 'A3P','Viernes','20:00', '14-03-2021', null),
		('29313095', 'A4P','Viernes','22:00', '14-03-2021', null),
		('30196994', 'A5P' ,'Lunes','17:00', '14-03-2021', null),
		('30196994', 'A6P' ,'Lunes','19:00', '14-03-2021', null),
		('35706114', 'I1V',  'Martes','19:00', '14-03-2021', null),
		('35706114', 'I2V',  'Martes','17:00', '14-03-2021', null),
		('35706114', 'I3V', 'Miercoles','17:00',  '14-03-2021', null),
		('35706114', 'I4V',  'Miercoles','19:00', '14-03-2021', null),
		('34586379',  'I5V', 'Jueves','17:00', '14-03-2021', null),
		('34586379',  'I6V', 'Jueves','19:00', '14-03-2021', null)
----------------------------------------------------------------------------------------------------------------

insert into eval_alumnos values

('35706249', 1, 8, '10-12-2020', false),
('14604391', 1, 6, '10-12-2020', false),
('35164380', 1, 7, '10-12-2020', false),

('35708167',2, 9, '11-12-2020', false),
('37292002',2, 6, '11-12-2020', false),
('37080626', 2, 8, '11-12-2020', false),
('32188504', 2, 10, '11-12-2020', false),

('20096213', 3, 6, '12-12-2020', false),
('23996958', 3, 7, '12-12-2020', false),
('35446262', 3, 7, '12-12-2020', false),

('31232287', 4, 8, '13-12-2020', false),
('34972735', 4, 8, '13-12-2020', false),
('35441760', 4, 9, '13-12-2020', false),
('33424056', 4, 10, '13-12-2020', false),

('31760315', 5, 6, '14-12-2020', false),
('27466071', 5, 8, '14-12-2020', false),



('34678065', 7, 6, '18-12-2020', false),
('32802059', 7, 9, '18-12-2020', false),


('30175494',8, 7, '19-12-2020', false),
('24592441',8, 8, '19-12-2020', false),
('36452907', 8, 10, '19-12-2020', false),
('34669622', 8, 9, '19-12-2020', false),

('26713163', 9, 7, '20-12-2020', false),
('35442432', 9, 8, '20-12-2020', false),
('21411913', 9, 6, '20-12-2020', false),
('36269182', 9, 9, '20-12-2020', false),

('33349258', 10, 7, '21-12-2020', false),
('25307515', 10, 8, '21-12-2020', false),
('30185183', 10, 8, '21-12-2020', false),
('35289179', 10, 6, '21-12-2020', false),

('34452406', 11, 9, '22-12-2020', false),
('28676029', 11, 10, '22-12-2020', false),

('32058807', 13, 9, '11-12-2020', false),
('23190739', 13, 8, '11-12-2020', false),

('34549589', 14, 7, '12-12-2020', false),
('33271242', 14, 8, '12-12-2020', false),

('35441206', 15, 9, '13-12-2020', false),
('32114383', 15, 6, '13-12-2020', false),

('33927404', 16, 10, '14-12-2020', false),
('34678024', 16, 8, '14-12-2020', false),
('27139929', 16, 9, '14-12-2020', false),
('32745392', 16, 8, '14-12-2020', false),

('35442295', 17, 8, '15-12-2020', false),
('31439635', 17, 9, '15-12-2020', false)


------------------------------------------------------------------------------------------------------------------

insert into pagos (cod_sede, dni_alumno,  cod_cursoidioma,monto) values
('3100','29447407', 'E1P', 2000),
		('3100','32327660', 'E1P', 2000),
		('3100','20096170', 'E1P', 2000),
		('3100','34059089', 'E1P', 2000),

		('3100','35706249', 'E2P', 2100),
		('3100','14604391', 'E2P', 2100),
		('3100','35164380', 'E2P', 2100),

		('2000','35708167', 'E3V', 2200),
		('2000','37292002', 'E3V', 2200),
		('2000','37080626', 'E3V', 2200),
		('2000','32188504', 'E3V', 2200),

		('2000','20096213', 'E4V', 2300),
		('2000','23996958', 'E4V', 2300),
		('2000','35446262', 'E4V', 2300),

		('5500','31232287', 'E5P', 2400),
		('5500','34972735', 'E5P', 2400),
		('5500','35441760', 'E5P', 2400),
		('5500','33424056', 'E5P', 2400),

		('5500','31760315', 'E6P', 2500),
		('5500','27466071', 'E6P', 2500),

		('5500','29447170', 'A1V', 2000),
		('5500','26203750', 'A1V', 2000),
		('5500','36910091', 'A1V', 2000),
		('5500','35295276', 'A1V', 2000),

		('3100','34678065', 'A2P', 2100),
		('3100','32802059', 'A2P', 2100),


		('3100','30175494','A3P', 2200),
		('3100','24592441','A3P', 2200),
		('3100','36452907', 'A3P', 2200),
		('3100','34669622', 'A3P', 2200),

		('3100','26713163', 'A4V', 2300),
		('3100','35442432', 'A4V', 2300),
		('3100','21411913', 'A4V', 2300),
		('3100','36269182', 'A4V', 2300),
		
		('2000','33349258', 'A5V', 2400),
		('2000','25307515', 'A5V', 2400),
		('2000','30185183', 'A5V', 2400),
		('2000','35289179', 'A5V', 2400),
		('2000','34452406', 'A6P', 2500),
		('2000','28676029', 'A6P', 2500),
		('2000','31017257', 'I1P', 2000),
		('2000','34821454', 'I1P', 2000),
		('2000','31384596', 'I1P', 2000),
		('2000','32833780', 'I1P', 2000),
		('3100','32058807', 'I2V', 2100),
		('3100','23190739', 'I2V', 2100),
		('3100','34549589', 'I3V', 2200),
		('3100','33271242', 'I3V', 2200),
		('3100','35441206', 'I4V', 2300),
		('3100','32114383', 'I4V', 2300),
		('5500','33927404', 'I5P', 2400),
		('5500','34678024', 'I5P', 2400),
		('5500','27139929', 'I5P', 2400),
		('5500','32745392', 'I5P', 2400),
		('5500','35442295', 'I6V', 2500),
		('5500','31439635', 'I6V', 2500)




----------------------Comienzo del desarrollo-----------------------------------------------------------

create or replace function agregar_registros() returns void as
$$
begin
	begin
		insert into sedes values
		('3100', 'Entre Rios', 'Paraná', 'Necochea 553', '+543434242791'),
		('2000', 'Santa Fe', 'Rosario', 'Peron 123', '+543414657802'),
		('5500', 'Mendoza', 'Mendoza', 'San Martin 456', '+542614316280');
	end;
	
	begin
		insert into cursos values
		('E1P','Ingles', 'A1'),
		('E2P','Ingles', 'A2'),
		('E3P','Ingles', 'B1'),
		('E4P','Ingles', 'B2'),
		('E5P','Ingles', 'C1'),
		('E6P','Ingles', 'C2'),
		('A1P','Aleman', 'A1'),
		('A2P','Aleman', 'A2'),
		('A3P','Aleman', 'B1'),
		('A4P','Aleman', 'B2'),
		('A5P','Aleman', 'C1'),
		('A6P','Aleman', 'C2'),
		('I1P','Italiano', 'A1'),
		('I2P','Italiano', 'A2'),
		('I3P','Italiano', 'B1'),
		('I4P','Italiano', 'B2'),
		('I5P','Italiano', 'C1'),
		('I6P','Italiano', 'C2'),
		('E1V','Ingles', 'A1'),
		('E2V','Ingles', 'A2'),
		('E3V','Ingles', 'B1'),
		('E4V','Ingles', 'B2'),
		('E5V','Ingles', 'C1'),
		('E6V','Ingles', 'C2'),
		('A1V','Aleman', 'A1'),
		('A2V','Aleman', 'A2'),
		('A3V','Aleman', 'B1'),
		('A4V','Aleman', 'B2'),
		('A5V','Aleman', 'C1'),
		('A6V','Aleman', 'C2'),
		('I1V','Italiano', 'A1'),
		('I2V','Italiano', 'A2'),
		('I3V','Italiano', 'B1'),
		('I4V','Italiano', 'B2'),
		('I5V','Italiano', 'C1'),
		('I6V','Italiano', 'C2');

	end;
	
			begin
		insert into personas values
		('32831554', 'Martin','Estrada','21-02-1987','Alsina 869', '3434636602'),
		('37080129', 'Analia Belén','Cuenca','03-09-1992','Garrigó 1675 ', '3435029905'),
		('35706276', 'Alejandro','Ghiringhelli','02-04-1991','Diamante 267', '3436230863'),
		('31484774','Alfredo Francisco', 'Machado','01/06/1985', 'BoJ. Hernandez Manz. 4 C 2',  '4390678'),
		('30563991','Ezequiel Eduardo','Mandel','18/08/1962', 'Gessino 216 Casa 14 Mzna. 4','4354128' ),
		('30796852', 'Ana Estela','Ortiz','24/06/1985','Chacabuco 484','154502946' ),
		('17022037', 'Omar Eliseo','Pitrovski','23/11/1980', 'Lomas del Mirador II Sec. A2','4246192' ),
		('30560373','Xavier Emmanuel','Ramirez','18/08/1974', 'J. J. Hernandez Cuerpo 6 Mzna. 4','15537238'),
		('32114702','Dario', 'Renna','13/08/1982','Barrio VICOER XII Mzna. A','0344287348' ),
		('26152765','Emmanuel','Retamar','07/10/1982', 'BoHernandarias', '0315614420'  ),
		('29269732', 'Gabriel Eugenio','Sosa','26/03/1986', 'Villaguay', '4341273' ),
		('35441479','Guido Leonel','Suarez Rios','24/07/1981','Ayacucho 564',null ),
		('29855862', 'Patricio Jose','Taurisano','25/10/1984', 'Santiago Derqui 749','4374410'),
		('29957038','Pascual Timoteo A.','Uriona','11/03/1990', 'BoParana I Mzna 1','154553802' ),
		('22850572','Leandro Raul','Vacaretti','25/06/1981', 'Nicanor Molina 511','4340448' ),
		('22160011','Baltazar Carlos Eduardo','Wolff','04/07/1955', 'Jose M. Paz 1097','156238333' ),
		('31277508','Leonardo Javier','Caceres','02/11/1982', 'Santiago Derqui 749','4840430' ),
		('32188553', 'Javier','Filgueira','26/01/1984', 'Barrio Parana XIV Mzna. C','4390676' ),
		('29157553','Angel Santiago','Flores','16/07/1978', 'BoJ.M. de Rosas Mzna. C2 Dpto 1','155042142' ),
		('27337776','Juan Manuel','Iglesias','13/03/1984', 'La Rioja 331', '154684007' ),
		('31579005','Luis Alejo','Romani','30/03/1988', 'Antonio Machado 703','156230784' ),
		('33865514','Juan Pablo','Yacob','03/03/1977', 'Houssay 415', '154292170'),
		('29447624','Luciano Miguel','Yugdar','24/07/1983', 'Barrio Parana XIV','154520300' ),
		('28913190','Cynthia Noemi','Zacarias','06/07/1978', 'Berutti 640','154291709' ),
		('29447678','Ivan Sergio Raul','Zanin','19/08/1982', 'Nicanor Molina 511','154786824' ),
		('29945156','Maximiliano Juan Jose','Zapata','29/09/1982', 'Dr. Gessino 556','154297028' ),
		('35441056','Cristhian Orlando','Diana','14/01/1988', 'Cordoba 162','0315590264'),
		('36813450', 'Gaston Edurado', 'Perez', '23/10/1992', 'Laprida 751', '02915408269'),
		('30578551', 'Jonathan Fabricio', 'Osorio', '03/11/1983', 'Los Paraisos 75', '4975377'),
		('24752862', 'Federico Ivan', 'Pagura', '03/07/1975', 'Alsina 637', '03156234277'),
		('25089771', 'Raul Virgilio', 'Rodriguez', '04/02/1976', 'Monte Caseros 323', '4227355'),
		('36813449', 'Walter German Ramon', 'Wurstten', '13/02/1982', 'Laprida 760', '156117046'),
		('21698023', 'Leonardo Juan Jose', 'Almada', '27/05/1970', 'Los Paraisos 75', '155032229'),
		('32932173', 'Hernan Dario', 'Schmittlein', '23/10/1992', 'Alsina 654', '03156356487'),
		('25089776', 'Gonzalo Exequiel', 'Saavedra', '25/02/1988', 'Rep. Dominicana 552', '155091007'),
		('30408500', 'Gabriel Edurado', 'Martinez', '23/10/1992', 'Laprida 758', '02915408269'),
		('29447005', 'Paola Soledad ', 'Roble' , '27/03/1982', 'Saravi 117', '4373580'),
		('34586153', 'Yamila Haydee', 'Quintana', '16/07/1989', 'Los Jacarandaes 1173', '154732280'),
		('26564401', 'Cynthia Carina', 'Popelka', '17/07/1978', 'Nogoya 49', '4231234'),
		('31313058', 'Juan Esteban','Piaggio','13/12/1984', 'Carrera y Vidal', NULL),
		('25546396', 'Luis Maria', 'Piani', '26/11/1976', 'Moises Lebenshon 3426', '4351142'),
		('34549524', 'Hugo Alejandro','Piantanida', '03/06/1989', 'Rio de los Pajaros 2716', '4331382'),
		('37465542', 'Nicolas Daniel', 'Piccoli', '03/04/1993', 'Coronel Caminos 1329','4272812'),
		('26519505', 'Soledad Lucrecia','Piccolo', '09/04/1978', 'Los Sauces 162', NULL),
		('33838416', 'Alejandro Gabriel','Pico', '06/06/1988','Casiano Calderon 1888', '4272079'),
		('27449041', 'Gabriel Jesus', 'Picotte', '10/10/1979', 'Virgen del Lujan 1951', '4271193'),
		('29313095', 'Jorge Jesus','Picotte', '20/07/1982',  'Virgen del Lujan 1951', '4271193'),
		('30196994', 'Maria De Los Angeles','Picotti', '25/02/1984', 'Pte. Peron 545', '4930302'),
		('30322973', 'Matias Ismael','Picotti', '11/04/1984', '9 de julio 124',  '034156042'),
		('28968427', 'Javier Luis','Pidone', '28/09/1981', 'Buenos Aires 441',  '4231109'),
		('32509756', 'Ekaterina Micaela','Piergiovanni', '20/08/1986',  'Suipacha 1867', '4241228'),
		('35706114', 'Miguel Martin','Piergiovanni', '13/03/1991',  'Gonzalez Pecotche 1896','4241228'),
		('34586379', 'Mariano Hernan','Piffiguer', '04/09/1989', 'Dragones de Entre Rios 635', '154767142'),
		('35448155', 'Juan Antonio','Pighin', '07/08/1990','A. Castellano 2097', '421779'),
		('14330569', 'Walter Carlos', 'Pignatta', '05/09/1961', 'Ameghino 581','034432156'),
		('29447407', 'Federico','Pilnik', '20/06/1982', 'San Martin 118','4314439'),
		('32327660', 'Bruno Andres','Pimentel', '02/04/1986',  'Los Ceibos 869', '4271012'),
		('35706249', 'Maria Florencia','Pimentel', '27/03/1991', 'Los Ceibos 869', '03470805'),
		('14604391', 'Raul Ceferino','Pin', '29/01/1961','Rep. Dominicana 640','4340487'),
		('35708167', 'Micaela Anabel','Pineiro', '28/07/1991', 'Guemes 255', '03438421396'),
		('37292002', 'Pedro Gustavo', 'Pineyro Santucci', '15/10/1992', 'Zona Rural 0', '037420511'),
		('20096213', 'Raul Francisco', 'Pintos Sors', '15/03/1968','Rosario del Tala 512', '4221686'),
		('23996958', 'Claudio Fabian','Pintos', '09/09/1974', 'Alvarado 2684',  '156217841'),
		('31232287', 'Juan Emanuel','Pintos', '21/11/1984', 'Bvar. Sarmiento 579',  '4221239'),
		('34972735', 'Carlos Eduardo de Jesus', 'Piris',  '26/12/1989', 'Gobernador Antelo 1683',  '154407421'),
		('31760315', 'Vanina Maria Alejandra', 'Pirola', '06/08/1985',  'Los Sauces 243',  '4260625'),
		('27466071', 'Pablo Martin','Pittavino', '14/07/1979', 'Las Lechiguanas 597', '4243187'),
		('29447170', 'Nestor Ivan','Pizzichini', '08/04/1982',  'Regis Martinez 1735', '4246437'),
		('26203750', 'Marianela','Pizzio', '16/11/1977',  'Gob. Parera 985', '4260093'),
		('34678065', 'Diego Emanuel', 'Planes', '04/07/1989', 'La Delfina 1441', '4953903' ),
		('32802059', 'Leandro Enrique', 'Planes', '19/05/1987', 'Turi 97','155035854'),
		('30175494', 'Federico','Plaumer', '11/12/1984',  'Las Lechiguanas 571','4247784'),
		('24592441', 'Claudia Rosana','Plez', '23/07/1975','Berio Acosta 1625', '155449461'),
		('21411913', 'Cristian Raul','Pochon', '05/01/1970', 'Espana 267', '154476178'),
		('36269182', 'Bruno Javier', 'Podversich', '20/01/1992', 'Zona Rural 0', '034154526051'),
		('33349258', 'Eugenio Ezequiel','Podversich', '12/05/1988', 'Gervacio Mendez 519', '154388815'),
		('25307515', 'Fernan Humberto','Poidomani', '01/08/1976', 'Cordoba 740','4317337'),
		('34452406', 'Adrian Eduardo', 'Polarrolo', '15/09/1989', 'Ecuador 138', '4910435'),
		('28676029', 'Graciela Valeria','Politi', '11/02/1981', 'Av. de las Americas', '4350114'),
		('31017257', 'Exequiel Maximiliano','Polito Acosta', '28/07/1984',   'Gendarmeria Nacional 1637','4030909'),
		('34821454', 'Franco Joel', 'Polo', '12/02/1990','Iriondo 1581','0348229033'),
		('32058807', 'Laura', 'Ponce Bossio', '20/07/1986','San Martin 772',  '4973301'),
		('23190739', 'Lucrecia Margarita Mercedes','Ponce', '09/08/1973', 'Independencia 958', '4880490'),
		('34549589', 'Pablo Cesar','Ponce', '21/07/1989',  'Badano 0','154623501'),
		('33271242', 'Francisco','Pons', '20/10/1987', '3 de Febrero', '4248909'),
		('35441206', 'Juan Carlos','Poos', '15/11/1990', 'Coronel Uzin 555', '4349858'),
		('32114383', 'Exequiel Dario', 'Popp', '26/03/1986','Misiones 1341', '49503502'),
		('33927404', 'Gisela Mariel','Popp', '29/05/1988',  'Pasaje Moreno 74', '154388247'),
		('34678024', 'Ivana Gisel','Popp', '05/06/1989', 'Misiones 1341', '4953502'),
		('35442295', 'Jennifer Stefania', 'Popp', '29/09/1990', 'Rodriguez Pena 1034', '155048353'),
		('31439635', 'Luciano','Porcaro', '25/02/1985',  'Av. Zanni 891', '4240872'),
		('20096170', 'Orlando Javier','Porchnek', '24/02/1968', 'Av. Ramirez 3361', '4245155'),
		('34059089', 'Mayka Antonella','Portal', '17/06/1990',  'Houssay 968',  '4910534'),
		('37080626', 'Fabiola Maria del Mar','Portillo', '30/09/1992',  'Repetto 3804','0345422566'),
		('32188504', 'Fabian Daniel', 'Portnoy', '18/02/1986', 'Almafuerte 225', '4245874'),
		('35441760', 'Guillermo Luis','Portnoy', '22/01/1991', 'Almafuerte 225', '156111159'),
		('33424056', 'Emanuel Maria','Porto Pereira', '15/12/1987', 'French 530','03156209229'),
		('35164380', 'Marlene', 'Porto Pereira', '25/07/1990', 'French 530', '4994079'),
		('35446262', 'Cristian Ezequiel','Portorreal', '07/08/1990',  'Pellegrini 1443','034754233'),
		('30185183', 'Rodrigo Daniel','Postigo Werbrouck', '25/06/1983',  'Churruarin 525', '03156309443'),
		('35289179', 'Milton', 'Pozzo', '14/09/1990','Belgrano 2548',  '03496-428955'),
		('31384596', 'Nicolas Nazareno','Pozzo', '08/05/1985', 'Rio de Janeiro 76', '034430337'),
		('32833780', 'Pablo Emanuel','Prada', '20/06/1987',   'Tomas Guido 2177', '4373172'),
		('36910091', 'Geronimo Fabian','Pradella', '13/05/1992',  'Piran 5825', '4270220'),
		('35295276', 'Daniel', 'Prado', '13/05/1990', 'Cuba 56', '4225161'),
		('27139929', 'Danilo Martin','Pralong', '21/03/1979',   'Courrege 241', '4229766'),
		('32745392', 'Sebastian Efrain', 'Pralong', '12/02/1987',  'H. Irigoyen 521', '4218164'),
		('36452907', 'Agustin','Prediger', '13/03/1992',   'BoSan Salvador Casa 212',  '2901431913'),
		('34669622', 'German', 'Prediger', '28/09/1989', 'Barrio San Salvador 212',  '029431913'),
		('26713163', 'Andrea Marisel', 'Preisz', '29/01/1979', 'Diaz Colodrero 414',  '154285095'),
		('35442432', 'Ariel Fernando', 'Preisz', '09/09/1990', 'Alejo Peyret 313',  '4235446');
	end;
	
	begin

		insert into cargos values
		('17022037', null, null, 'DG', '20-03-2022' ),
		('37080129', '3100', 'Ingles', 'DI', '10-03-2022' ),
		('35706276', '3100', 'Aleman', 'DI' , '11-03-2022'),
		('32831554', '3100', 'Italiano', 'DI' , '12-03-2022'),
		('26564401', '2000', 'Ingles', 'DI', '13-03-2022' ),
		('34586153', '2000', 'Aleman', 'DI' , '14-03-2022'),
		('29447005', '2000', 'Italiano', 'DI' , '15-03-2022'),
		('30408500', '5500', 'Ingles', 'DI' , '16-03-2022'),
		('25089776', '5500', 'Aleman', 'DI' , '17-03-2022'),
		('32932173', '5500', 'Italiano', 'DI' , '18-03-2022'),
		('21698023', '3100', 'Ingles', 'SI' , '19-03-2022'),
		('36813449', '3100', 'Aleman', 'SI' , '20-03-2022'),
		('25089771', '3100', 'Italiano', 'SI' , '21-03-2022'),
		('24752862', '2000', 'Ingles', 'SI', '22-03-2022' ),
		('30578551', '2000', 'Aleman', 'SI', '23-03-2022' ),
		('36813450', '2000', 'Italiano', 'SI' , '24-03-2022'),
		('29447678', '5500', 'Ingles', 'SI', '25-03-2022' ),
		('29945156', '5500', 'Aleman', 'SI', '26-03-2022' ),
		('35441056', '5500', 'Italiano', 'SI', '27-03-2022' ),
		('31484774','3100', 'Ingles', 'DO', '10-03-2022' ),
		('30563991','3100', 'Ingles', 'DO', '10-03-2022' ),
		('30796852', '3100', 'Ingles', 'EV', '10-03-2022' ),
		('31313058', '3100', 'Ingles', 'EV', '10-03-2022' ),
		('30560373', '3100', 'Aleman', 'DO' , '14-03-2022'),
		('32114702', '3100', 'Aleman', 'DO' , '14-03-2022'),
		('26152765', '3100', 'Aleman', 'EV' , '14-03-2022'),
		('29269732',  '3100', 'Aleman', 'EV' , '14-03-2022'),
		('35441479', '3100', 'Italiano', 'DO' , '14-03-2022'),
		('29855862',  '3100', 'Italiano', 'DO' , '14-03-2022'),
		('29957038', '3100', 'Italiano', 'EV' , '14-03-2022'),
		('22850572', '3100', 'Italiano', 'EV' , '14-03-2022'),
		('22160011', '2000', 'Ingles', 'DO' , '14-03-2022'),
		('31277508', '2000', 'Ingles', 'DO' , '14-03-2022'),
		('32188553', '2000', 'Ingles', 'EV', '10-03-2022' ),
		('29157553', '2000', 'Ingles', 'EV', '10-03-2022' ),
		('27337776', '2000', 'Aleman', 'DO' , '14-03-2022'),
		('31579005', '2000', 'Aleman', 'DO' , '14-03-2022'),
		('33865514','2000', 'Aleman', 'EV' , '14-03-2022'),
		('29447624','2000', 'Aleman', 'EV' , '14-03-2022'),
		('28913190','2000', 'Italiano', 'DO' , '14-03-2022'),
		('31313058', '2000', 'Italiano', 'DO' , '14-03-2022'),
		('25546396','2000', 'Italiano', 'EV' , '14-03-2022'),
		('34549524','2000', 'Italiano', 'EV' , '14-03-2022'),
		('37465542','5500', 'Ingles', 'DO', '10-03-2022' ),
		('26519505','5500', 'Ingles', 'DO', '10-03-2022' ),
		('33838416', '5500', 'Ingles', 'EV', '10-03-2022' ),
		('27449041', '5500', 'Ingles', 'EV', '10-03-2022' ),
		('29313095', '5500', 'Aleman', 'DO' , '14-03-2022'),
		('30196994', '5500', 'Aleman', 'DO' , '14-03-2022'),
		('30322973', '5500', 'Aleman', 'EV' , '14-03-2022'),
		('32509756',  '5500', 'Aleman', 'EV' , '14-03-2022'),
		('35706114', '5500', 'Italiano', 'DO' , '14-03-2022'),
		('34586379',  '5500', 'Italiano', 'DO' , '14-03-2022'),
		('35448155', '5500', 'Italiano', 'EV' , '14-03-2022'),
		('14330569', '5500', 'Italiano', 'EV' , '14-03-2022');
	end;
	
	begin		
		insert into evaluaciones (dni_docente_evaluador,cod_cursoidioma,fecha) values 
			('31484774','E1P', '10-12-2022' ),
			('31484774','E2P','11-12-2022'),
			('31484774','E3P','12-12-2022' ),
			('31484774','E4P', '13-12-2022' ),
			('30563991','E5P','14-12-2022' ),
			('30563991','E6P', '15-12-2022' ),
			('30560373', 'A1P', '18-12-2022'),
			('30560373', 'A2P', '19-12-2022'),
			('30560373', 'A3P', '20-12-2022'),
			('30560373', 'A4P', '21-12-2022'),
			('32114702', 'A5P', '22-12-2022'),
			('32114702', 'A6P', '10-12-2022'),
			('35441479', 'I1P', '11-12-2022'),
			('35441479', 'I2P', '12-12-2022'),
			('35441479', 'I3P',  '13-12-2022'),
			('35441479', 'I4P', '14-12-2022'),
			('29855862',  'I5P', '15-12-2022'),
			('29855862',  'I6P', '18-12-2022');
	
	end;
	
	begin
			Insert into curso_alumnos values
			('29447407', 'E1P', 'Lunes','20:00', '04-03-2021', null),
			('32327660', 'E1P', 'Lunes','20:00', '04-03-2021', null),
			('20096170', 'E1P', 'Lunes','20:00', '04-03-2021', null),
			('34059089', 'E1P', 'Lunes','20:00', '04-03-2021', null),
			('35706249', 'E2P', 'Lunes','22:00', '04-03-2021', null),
			('14604391', 'E2P', 'Lunes','22:00', '04-03-2021', null),
			('35164380', 'E2P', 'Lunes','22:00', '04-03-2021', null),
			('35708167', 'E3V', 'Martes','20:00', '04-03-2021', null),
			('37292002', 'E3V', 'Martes','20:00', '04-03-2021', null),
			('37080626', 'E3V', 'Martes','20:00', '04-03-2021', null),
			('32188504', 'E3V', 'Martes','20:00', '04-03-2021', null),
			('20096213', 'E4V', 'Martes','22:00', '04-03-2021', null),
			('23996958', 'E4V', 'Martes','22:00', '04-03-2021', null),
			('35446262', 'E4V', 'Martes','22:00', '04-03-2021', null),
			('31232287', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
			('34972735', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
			('35441760', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
			('33424056', 'E5P', 'Miercoles','20:00', '04-03-2021', null),
			('31760315', 'E6P', 'Miercoles','22:00', '04-03-2021', null),
			('27466071', 'E6P', 'Miercoles','22:00', '04-03-2021', null),
			('29447170', 'A1V', 'Jueves','20:00', '04-03-2021', null),
			('26203750', 'A1V', 'Jueves','20:00', '04-03-2021', null),
			('36910091', 'A1V', 'Jueves','20:00', '04-03-2021', null),
			('35295276', 'A1V', 'Jueves','20:00', '04-03-2021', null),
			('34678065', 'A2P', 'Jueves','22:00', '04-03-2021', null),
			('32802059', 'A2P', 'Jueves','22:00', '04-03-2021', null),
			('30175494','A3P', 'Viernes','20:00', '04-03-2021', null),
			('24592441','A3P', 'Viernes','20:00', '04-03-2021', null),
			('36452907', 'A3P', 'Viernes','20:00', '04-03-2021', null),
			('34669622', 'A3P', 'Viernes','20:00', '04-03-2021', null),
			('26713163', 'A4V', 'Viernes','22:00', '04-03-2021', null),
			('35442432', 'A4V', 'Viernes','22:00', '04-03-2021', null),
			('21411913', 'A4V', 'Viernes','22:00', '04-03-2021', null),
			('36269182', 'A4V', 'Viernes','22:00', '04-03-2021', null),
			('33349258', 'A5V', 'Lunes','17:00', '04-03-2021', null),
			('25307515', 'A5V', 'Lunes','17:00', '04-03-2021', null),
			('30185183', 'A5V', 'Lunes','17:00', '04-03-2021', null),
			('35289179', 'A5V', 'Lunes','17:00', '04-03-2021', null),
			('34452406', 'A6P', 'Lunes','19:00', '04-03-2021', null),
			('28676029', 'A6P', 'Lunes','19:00', '04-03-2021', null),
			('31017257', 'I1P', 'Martes','19:00', '04-03-2021', null),
			('34821454', 'I1P', 'Martes','19:00', '04-03-2021', null),
			('31384596', 'I1P', 'Martes','19:00', '04-03-2021', null),
			('32833780', 'I1P', 'Martes','19:00', '04-03-2021', null),
			('32058807', 'I2V', 'Martes','17:00', '04-03-2021', null),
			('23190739', 'I2V', 'Martes','17:00', '04-03-2021', null),
			('34549589', 'I3V', 'Miercoles','17:00', '04-03-2021', null),
			('33271242', 'I3V', 'Miercoles','17:00', '04-03-2021', null),
			('35441206', 'I4V', 'Miercoles','19:00', '04-03-2021', null),
			('32114383', 'I4V', 'Miercoles','19:00', '04-03-2021', null),
			('33927404', 'I5P', 'Jueves','17:00', '04-03-2021', null),
			('34678024', 'I5P', 'Jueves','17:00', '04-03-2021', null),
			('27139929', 'I5P', 'Jueves','17:00', '04-03-2021', null),
			('32745392', 'I5P', 'Jueves','17:00', '04-03-2021', null),
			('35442295', 'I6V', 'Jueves','19:00', '04-03-2021', null),
			('31439635', 'I6V', 'Jueves','19:00', '04-03-2021', null);

	end;
	
	begin
		insert into curso_docentes values
		--parana
		('31484774','E1P','Lunes','20:00', '10-03-2021', null ),
		('31484774','E2P','Lunes','22:00', '10-03-2021', null ),
		('31484774','E3V','Martes','20:00', '10-03-2021', null ),
		('31484774','E4V','Martes','22:00', '10-03-2021', null ),
		('30563991','E5P','Miercoles','20:00' ,'10-03-2021' , null),
		('30563991','E6P','Miercoles','22:00', '10-03-2021', null ),
		('30560373', 'A1V' ,'Jueves','20:00', '14-03-2021', null),
		('30560373', 'A2P' ,'Jueves','22:00', '14-03-2021', null),
		('30560373', 'A3P' ,'Viernes','20:00', '14-03-2021', null),
		('30560373', 'A4V' ,'Viernes','22:00', '14-03-2021', null),
		('32114702', 'A5V' ,'Lunes','17:00', '14-03-2021', null),
		('32114702', 'A6P' ,'Lunes','19:00', '14-03-2021', null),
		('35441479', 'I1P', 'Martes','19:00', '14-03-2021', null),
		('35441479', 'I2V', 'Martes','17:00', '14-03-2021', null),
		('35441479', 'I3V','Miercoles','17:00',  '14-03-2021', null),
		('35441479', 'I4V', 'Miercoles','19:00', '14-03-2021', null),
		('29855862',  'I5V' ,'Jueves','17:00', '14-03-2021', null),
		('29855862',  'I6V' ,'Jueves','19:00', '14-03-2021', null),
		--Rosario
		('22160011','E1P','Lunes','20:00', '10-03-2021', null ),
		('22160011','E2P', 'Lunes','22:00', '10-03-2021', null ),
		('22160011','E3P', 'Martes','20:00', '10-03-2021', null ),
		('22160011','E4P', 'Martes','22:00', '10-03-2021', null ),
		('31277508','E5P', 'Miercoles','20:00' ,'10-03-2021' , null),
		('31277508','E6P', 'Miercoles','22:00', '10-03-2021', null ),
		('27337776', 'A1V','Jueves','20:00', '14-03-2021', null),
		('27337776', 'A2V','Jueves','22:00', '14-03-2021', null),
		('27337776', 'A3V','Viernes','20:00', '14-03-2021', null),
		('27337776', 'A4V','Viernes','22:00', '14-03-2021', null),
		('31579005', 'A5V' ,'Lunes','17:00', '14-03-2021', null),
		('31579005', 'A6V' ,'Lunes','19:00', '14-03-2021', null),
		('35441479', 'I1P',  'Martes','19:00', '14-03-2021', null),
		('35441479', 'I2P',  'Martes','17:00', '14-03-2021', null),
		('35441479', 'I3P', 'Miercoles','17:00',  '14-03-2021', null),
		('35441479', 'I4P',  'Miercoles','19:00', '14-03-2021', null),
		('29855862',  'I5P', 'Jueves','17:00', '14-03-2021', null),
		('29855862',  'I6P', 'Jueves','19:00', '14-03-2021', null),
		--mendoza
		('37465542','E1V','Lunes','20:00', '10-03-2021', null ),
		('37465542','E2V', 'Lunes','22:00', '10-03-2021', null ),
		('37465542','E3V', 'Martes','20:00', '10-03-2021', null ),
		('37465542','E4V', 'Martes','22:00', '10-03-2021', null ),
		('26519505','E5V', 'Miercoles','20:00' ,'10-03-2021' , null),
		('26519505','E6V', 'Miercoles','22:00', '10-03-2021', null ),
		('29313095', 'A1P','Jueves','20:00', '14-03-2021', null),
		('29313095', 'A2P','Jueves','22:00', '14-03-2021', null),
		('29313095', 'A3P','Viernes','20:00', '14-03-2021', null),
		('29313095', 'A4P','Viernes','22:00', '14-03-2021', null),
		('30196994', 'A5P' ,'Lunes','17:00', '14-03-2021', null),
		('30196994', 'A6P' ,'Lunes','19:00', '14-03-2021', null),
		('35706114', 'I1V',  'Martes','19:00', '14-03-2021', null),
		('35706114', 'I2V',  'Martes','17:00', '14-03-2021', null),
		('35706114', 'I3V', 'Miercoles','17:00',  '14-03-2021', null),
		('35706114', 'I4V',  'Miercoles','19:00', '14-03-2021', null),
		('34586379',  'I5V', 'Jueves','17:00', '14-03-2021', null),
		('34586379',  'I6V', 'Jueves','19:00', '14-03-2021', null);	
	end;
	
	begin
			
		
		insert into eval_alumnos values
		('35708167',2, 9, '11-12-2020', false),
		('37292002',2, 6, '11-12-2020', false),
		('37080626', 2, 8, '11-12-2020', false),
		('32188504', 2, 10, '11-12-2020', false),
		('20096213', 3, 6, '12-12-2020', false),
		('23996958', 3, 7, '12-12-2020', false),
		('35446262', 3, 7, '12-12-2020', false),
		('31232287', 4, 8, '13-12-2020', false),
		('34972735', 4, 8, '13-12-2020', false),
		('35441760', 4, 9, '13-12-2020', false),
		('33424056', 4, 10, '13-12-2020', false),
		('31760315', 5, 6, '14-12-2020', false),
		('27466071', 5, 8, '14-12-2020', false),
		('34678065', 7, 6, '18-12-2020', false),
		('32802059', 7, 9, '18-12-2020', false),
		('30175494',8, 7, '19-12-2020', false),
		('24592441',8, 8, '19-12-2020', false),
		('36452907', 8, 10, '19-12-2020', false),
		('34669622', 8, 9, '19-12-2020', false),
		('26713163', 9, 7, '20-12-2020', false),
		('35442432', 9, 8, '20-12-2020', false),
		('21411913', 9, 6, '20-12-2020', false),
		('36269182', 9, 9, '20-12-2020', false),
		('33349258', 10, 7, '21-12-2020', false),
		('25307515', 10, 8, '21-12-2020', false),
		('30185183', 10, 8, '21-12-2020', false),
		('35289179', 10, 6, '21-12-2020', false),
		('34452406', 11, 9, '22-12-2020', false),
		('28676029', 11, 10, '22-12-2020', false),
		('32058807', 13, 9, '11-12-2020', false),
		('23190739', 13, 8, '11-12-2020', false),
		('34549589', 14, 7, '12-12-2020', false),
		('33271242', 14, 8, '12-12-2020', false),
		('35441206', 15, 9, '13-12-2020', false),
		('32114383', 15, 6, '13-12-2020', false),
		('33927404', 16, 10, '14-12-2020', false),
		('34678024', 16, 8, '14-12-2020', false),
		('27139929', 16, 9, '14-12-2020', false),
		('32745392', 16, 8, '14-12-2020', false),
		('35442295', 17, 8, '15-12-2020', false),
		('31439635', 17, 9, '15-12-2020', false),
		('35706249', 18, 8, '10-12-2020', false),
		('14604391', 18, 6, '10-12-2020', false),
		('35164380', 18, 7, '10-12-2020', false)
		;
	
	
	end;
	
	begin
		insert into pagos (cod_sede, dni_alumno,  cod_cursoidioma,monto) values
		('3100','29447407', 'E1P', 2000),
		('3100','32327660', 'E1P', 2000),
		('3100','20096170', 'E1P', 2000),
		('3100','34059089', 'E1P', 2000),
		('3100','35706249', 'E2P', 2100),
		('3100','14604391', 'E2P', 2100),
		('3100','35164380', 'E2P', 2100),
		('2000','35708167', 'E3V', 2200),
		('2000','37292002', 'E3V', 2200),
		('2000','37080626', 'E3V', 2200),
		('2000','32188504', 'E3V', 2200),
		('2000','20096213', 'E4V', 2300),
		('2000','23996958', 'E4V', 2300),
		('2000','35446262', 'E4V', 2300),
		('5500','31232287', 'E5P', 2400),
		('5500','34972735', 'E5P', 2400),
		('5500','35441760', 'E5P', 2400),
		('5500','33424056', 'E5P', 2400),
		('5500','31760315', 'E6P', 2500),
		('5500','27466071', 'E6P', 2500),
		('5500','29447170', 'A1V', 2000),
		('5500','26203750', 'A1V', 2000),
		('5500','36910091', 'A1V', 2000),
		('5500','35295276', 'A1V', 2000),
		('3100','34678065', 'A2P', 2100),
		('3100','32802059', 'A2P', 2100),
		('3100','30175494','A3P', 2200),
		('3100','24592441','A3P', 2200),
		('3100','36452907', 'A3P', 2200),
		('3100','34669622', 'A3P', 2200),
		('3100','26713163', 'A4V', 2300),
		('3100','35442432', 'A4V', 2300),
		('3100','21411913', 'A4V', 2300),
		('3100','36269182', 'A4V', 2300),
		('2000','33349258', 'A5V', 2400),
		('2000','25307515', 'A5V', 2400),
		('2000','30185183', 'A5V', 2400),
		('2000','35289179', 'A5V', 2400),
		('2000','34452406', 'A6P', 2500),
		('2000','28676029', 'A6P', 2500),
		('2000','31017257', 'I1P', 2000),
		('2000','34821454', 'I1P', 2000),
		('2000','31384596', 'I1P', 2000),
		('2000','32833780', 'I1P', 2000),
		('3100','32058807', 'I2V', 2100),
		('3100','23190739', 'I2V', 2100),
		('3100','34549589', 'I3V', 2200),
		('3100','33271242', 'I3V', 2200),
		('3100','35441206', 'I4V', 2300),
		('3100','32114383', 'I4V', 2300),
		('5500','33927404', 'I5P', 2400),
		('5500','34678024', 'I5P', 2400),
		('5500','27139929', 'I5P', 2400),
		('5500','32745392', 'I5P', 2400),
		('5500','35442295', 'I6V', 2500),
		('5500','31439635', 'I6V', 2500);
	end;
	begin
		
		insert into incidencias (tipo_incidente, cod_sede, estado) values
		('Problema con Docente curso E1P', '3100','Pendiente'),
		('Problema con Evaluador curso A2V','2000', 'Finalizado');		
	end;
	
 	begin 
 		insert into informe_incidencia values
  		(1, 'se esta trabajando en el problema'),
     	(2, 'El error del boton fue corregido');
 	end;
end;
$$
language 'plpgsql';



create or replace function truncate_tablas() returns void as
$$
begin
	begin
	
	truncate table informe_incidencia, pagos,incidencias, curso_docentes, curso_alumnos, eval_alumnos, evaluaciones, cargos, personas, cursos, sedes;
	perform setval('incidencias_cod_seq', 1);
	perform setval('evaluaciones_nro_eval_seq', 1);
	end;
	
end;
$$
language 'plpgsql';

------------------------------------------------------------------------------------------------

----B-1
create view nombre_apellido as
select p.apellido||', '|| p.nombre  as nombre_completo from personas p order by p.apellido

select * from nombre_apellido 

----B-2

--hicimos los correpondientes analisis de Pk y FK

--C-- Crear todos los usuarios y las sentencias DCL correspondientes


 ---Usuarios

create user UsuarioA with password '1234'

create user UsuarioB with password '2345'

create user UsuarioC with password '3456'

grant all on cargos, curso_alumnos, curso_docentes,
cursos, eval_alumnos, evaluaciones, incidencias, informe_incidencia,
pagos, personas,saldo_restante, sedes to UsuarioA

grant insert, update, select on incidencias to UsuarioB


grant insert, update, select on informe_incidencia to UsuarioC



-----------------------------------------------------------------------------------

--d) Generar los índices necesarios en todas las tablas de relaciones a fin de agilizar las 
--búsquedas por un miembro de la relación. Fundamente su inclusión en función de la 
--cardinalidad estimada.


create index idx_dnidoc_codcurso on evaluaciones(dni_docente_evaluador, cod_cursoidioma)

create index idx_nombre_apellido on personas (nombre desc, apellido desc ) 

create index idx_dnial_monto on pagos (dni_alumno asc, monto asc )

create index idx_prov_ciudad on sedes (provincia, cuidad)

create index idx_nivel on cursos(nivel) 






--Generar los siguientes tipos de vistas:
-- Una vista que contemple la cantidad de cursos (asincrónicos/asincrónicos) y 
--sus docentes, así como también la cantidad de alumnos en cada uno de ellos.

create view datos_cursos as
select c.cod_idioma as Codigo, c.nombre as Idioma, count(ca.cod_curso_idioma) as Alumnos, cd.dni_docente as dni , p.apellido||', '|| p.nombre as docente from cursos c
join curso_alumnos ca on( c.cod_idioma = ca.cod_curso_idioma )
join curso_docentes cd on( c.cod_idioma = cd.cod_curso_idioma) 
join personas p on( cd.dni_docente = p.dni)
group by  cd.dni_docente,c.cod_idioma, p.apellido, p.nombre

select * from datos_cursos

------------------------------------------------------
-- Vista que contenga, una fila por cada tabla del sistema y 2 columnas, una con el nombre 
--de tabla y otra con la cantidad de filas. Nota: Vista estática, cuando se agrega una nueva 
--tabla se debe agregar explícitamente a la vista


create view view_tablas as

select 'curso_alumnos' as Nombre_tabla, count(*) as cantidad_registros from curso_alumnos
union
select 'curso_docentes', count(*) from curso_docentes 
union
select 'cursos', count(*) from cursos
union
select 'eval_alumnos', count(*) from eval_alumnos
union
select 'evaluaciones', count(*) from evaluaciones
union
select 'pagos', count(*) from pagos
union
select 'personas', count(*) from personas
union
select 'roles', count(*) from cargos
union
select 'sedes', count(*) from sedes
union
select 'Incidencias', count (*) from incidencias
union
select 'Informe_incidencia', count (*) from informe_incidencia



select * from view_tablas
--------------------------------------------------------------------------------

-- Generar la vista denominada gerente_informes (Usuario C) que contenga todos los 
--informes con todas las incidencias durante un período determinado

create domain dom_estado varchar(20) not null check(value in('Pendiente','Anulado','Finalizado'))

create table incidencias(

	cod serial primary key,
	tipo_incidente varchar(50) not null,
	cod_sede varchar(4) references sedes(cod_sede),
	fecha_incidente  date default(current_date) ,
	estado dom_estado
	
)


create table informe_incidencia(

	cod_informe int references incidencias(cod),
	descripcion text not null,
	fecha_informe date default(current_date)
	
)


insert into incidencias (tipo_incidente,cod_sede, estado) values
('Problema con el Docente de catedra E1P', '3100', 'Finalizado' ),
('Problema con el docente de catedra A3V','2000', 'Pendiente')



insert into informe_incidencia values
(4, 'El docente fue reportado')




create view gerente_informes as
select i.*, ii.descripcion from informe_incidencia ii
right join incidencias i on(ii.cod_informe = i.cod )
where fecha_incidente between '2021-10-05' and '2021-11-13'


----------------------------------------------------------------------------------------------------------------------------
-- Función que reciba como parámetro de entrada un apellido y nombre y genere iniciales de 2 a 4 dígitos.

--sacar inicilaes de 2 a 4


create or replace function GenerarIniciales(nombre varchar(15), apellido varchar(15)) returns varchar(4) as 
$$
declare 
iniciales_nombre varchar(1);
iniciales_nombre2 varchar(1);
nombre_concat varchar(2);
inciales_apellido varchar(1);
inciales_apellido2 varchar(1);
apellido_concat varchar(2);
concat_iniciales varchar(4);
begin 
		
	iniciales_nombre = substring( (regexp_split_to_array(nombre, E'\\s+'))[1],1,1);	
	nombre_concat = iniciales_nombre;
	
	iniciales_nombre2 =substring( (regexp_split_to_array(nombre, E'\\s+'))[2],1,1);
	
 	if iniciales_nombre2 is not null then
 	nombre_concat = nombre_concat || iniciales_nombre2;
	end if;
	
	inciales_apellido =substring( (regexp_split_to_array(apellido, E'\\s+'))[1],1,1);
	apellido_concat = inciales_apellido;
 	
	inciales_apellido2 =substring( (regexp_split_to_array(apellido, E'\\s+'))[2],1,1);
	if inciales_apellido2 is not null then
 	apellido_concat = apellido_concat || inciales_apellido2;
 	end if;
	concat_iniciales = upper(nombre_concat) || upper(apellido_concat);
	
	return concat_iniciales ;
	
		
	
end;
$$
language 'plpgsql';

select GenerarIniciales(p.nombre, p.apellido ), p.nombre, p.apellido from personas p order by p.apellido asc




---------------------------------------------------------------------------------------------------------------------

-- Función que permita determinar el precio de un curso (solo curso), el neto de 
--descuentos, recibiendo como parámetro, al menos el DNI del alumno.


create or replace function traer_pagos(dni varchar(9), descuento decimal(10,2)) returns varchar(30) as
$$
declare
	rec record;
	monto1 decimal(10,2);
	concat varchar(30);
begin
	for rec in select monto from pagos where dni_alumno = dni
	loop
		monto1 = rec.monto * (descuento/100);
	end loop;
	
	concat = '$'||cast( monto1 as varchar(15) )|| ' descuento de: '|| cast(descuento as varchar(4)) ||'%';
	
	return  concat;
end;
$$
LANGUAGE plpgsql;

-- Llamado de la función

select traer_pagos('29447407', 20) -- segundo parametro, porcentaje de descuento


-------------------------------------------------------------------------------------------------

--Generar una función que dada la clave primaria de un curso genere la lista de 
--cursos correlativos que se deben aprobar para inscribirse.


create or replace function correlativas(cod_curso_var varchar(30)) returns Table
(
	cod_idioma varchar(4)
	
) as
$$

begin
		
	return query
	select  c.cod_idioma from cursos c where 
	substring(cod_curso_var,1,1) = substring(c.cod_idioma,1,1) and
	substring(cod_curso_var,3,1) = substring(c.cod_idioma, 3,1) and
	substring(cod_curso_var,2,1) > substring(c.cod_idioma, 2,1);

end;
$$
language 'plpgsql'



select correlativas('E5P')




--------------------------------------------------------------------------------------------------

--i) Generar los siguientes triggers.
-- Control de inscripciones según correlatividades y requisitos
	
		
create or replace function sp_control_correlativas() returns trigger as
$$
begin
		if not exists(			
				select e.nota, e.dni_alumno, ev.cod_cursoidioma from eval_alumnos e
				join evaluaciones ev on( ev.nro_eval = e.nro_eval)	
				where
				substring(ev.cod_cursoidioma,1,1) = substring(new.cod_curso_idioma,1,1) and
				substring(ev.cod_cursoidioma, 3,1) = substring (new.cod_curso_idioma,3,1)and
				substring(ev.cod_cursoidioma,2,1) < substring(new.cod_curso_idioma, 2,1) and
				cast(substring(ev.cod_cursoidioma,2,1) as int) = (cast(substring(new.cod_curso_idioma, 2,1) as int) -1) and
				e.dni_alumno = new.dni_alumno and e.nota >5)  then
				raise exception 'No tiene las materias correlativas aprobadas!';
		end if;
		return new;

end;
$$
LANGUAGE 'plpgsql';


create trigger tg_control_correlativas before insert on curso_alumnos
for each row execute procedure sp_control_correlativas();

select * from eval_alumnos
select * from evaluaciones 


insert into curso_alumnos values ('35706249', 'E3P', 'Lunes','22:00', '04-03-2021', null)



-------------------------------------------------------------------------------------------

--Trigger que administre el saldo resultante del estado de cuenta de cada 
--alumno.

	create table saldo_restante(

		dni_alumno varchar(12),
		cod_sede varchar(4),
		cod_cursoidioma varchar(4),
		fecha date,
		saldo_restante numeric(10,2) 


	)



create or replace function sp_saldo_restante() returns trigger as
$$
declare
vpagos numeric(10,2);
cantidad_pagos int;
vmonto_restante numeric(10,2);


begin

		select pa.monto into vpagos from pagos pa where pa.dni_alumno =new.dni_alumno;
	 	select count(pa.monto) into cantidad_pagos from pagos pa where 	pa.dni_alumno =new.dni_alumno;	
 		vmonto_restante = (vpagos*10) - (vpagos * cantidad_pagos); 
			
		if((select distinct  sa.dni_alumno from saldo_restante sa where dni_alumno = new.dni_alumno) is not null) then
			update saldo_restante set saldo_restante = vmonto_restante where dni_alumno = new.dni_alumno;
		end if;
		if((select distinct  sa.dni_alumno from saldo_restante sa where dni_alumno = new.dni_alumno) is null) then
			insert into saldo_restante values( new.dni_alumno, new.cod_sede, new.cod_cursoidioma, new.fecha, vmonto_restante  );
			
		end if;

return null;
end;
$$
language 'plpgsql'


create trigger tg_saldo_restante  after insert on pagos
for each row execute procedure sp_saldo_restante()

 insert into pagos (COD_SEDE, COD_CURSOIDIOMA, DNI_alumno, fecha, monto) values(3100, 'E2P','29447407', current_date, 2100 ) 
 
 
 ---------------------------------------------------------------------------------------------------------------------------------
 





